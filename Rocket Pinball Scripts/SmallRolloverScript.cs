using UnityEngine;
using System.Collections;

public class SmallRolloverScript : MonoBehaviour {
	
	public float activeDuration;
	public int thisPoints;
	
	private float activeTime;
	
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	AudioSource rolloverAudio;

	// Use this for initialization
	void Start () {
		
		activeTime = Time.time;
		
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
		
		rolloverAudio = GetComponent(typeof(AudioSource)) as AudioSource;
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Time.time <= activeTime)
			this.light.enabled = true;
		else {
			this.light.enabled = false;
			gameManagerComponent.smallRolloverManager(transform.name, false);
		}	
	}
	
	
	void OnTriggerEnter (Collider collidedObject) {
		
		if (collidedObject.transform.name == "Pinball(Clone)" 
			&& gameManagerComponent.getZoneIsAlive(transform.root.name)
			&& Time.time >= activeTime)
		{
			activeTime = Time.time + activeDuration;
			gameManagerComponent.setScore(thisPoints);
			gameManagerComponent.smallRolloverManager(transform.name, true);
			
			rolloverAudio.Play ();
		}
	}
}
