using UnityEngine;
using System.Collections;

public class KickerScript : MonoBehaviour {
	
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	public float bounciness;
	public int thisPoints;
	
	double haloTime;
	
	AudioSource kickerAudio;
	
	// Use this for initialization
	void Start () {
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
		haloTime = Time.time;
		kickerAudio = GetComponent(typeof(AudioSource)) as AudioSource;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate () {
		if (Time.time < haloTime)
			this.light.enabled = true;
		else
			this.light.enabled = false;		
	}
	
	void OnCollisionEnter (Collision collidedObject) {
		
		if ((collidedObject.gameObject.name == "Pinball(Clone)") && (gameManagerComponent.getZoneIsAlive(this.transform.root.name))){
			collidedObject.rigidbody.AddForce(this.transform.up * bounciness, ForceMode.VelocityChange);
			gameManagerComponent.setScore(thisPoints);
			haloTime = Time.time + 0.3;		
			kickerAudio.Play ();
		}
	}
}
