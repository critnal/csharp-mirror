using UnityEngine;
using System.Collections;

public class PowerUpScript : MonoBehaviour {
	
	GameObject gameManagerObject;
	GameManager gameManagerComponent;
	
	public Rigidbody pinball;
	
	public Texture defaultTexture,
					damageTexture,
					spawnTexture,
					seekingTexture,
					speedTexture,
					extraBallTexture;
	
	private string thisPowerUp;
	
	private int tempInt;
	
	private double thisCooldownTime;
	
	AudioSource powerUpAudio;

	// Use this for initialization
	void Awake () {
		thisPowerUp = "default";
		thisCooldownTime = Time.time;
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerComponent = gameManagerObject.GetComponent<GameManager>();
		powerUpAudio = GetComponent(typeof(AudioSource)) as AudioSource;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		if (Time.time > thisCooldownTime && gameManagerComponent.getZoneIsAlive(this.transform.root.name)){
		
			tempInt = gameManagerComponent.objectPowerUpCheck(this.gameObject.name);
			
			switch (tempInt)
			{
			case 0:
				thisPowerUp = "damage";
				this.renderer.material.mainTexture = damageTexture;
				break;
			case 1:
				thisPowerUp = "seeking";
				this.renderer.material.mainTexture = seekingTexture;
				break;
			case 2:
				thisPowerUp = "spawn";
				this.renderer.material.mainTexture = spawnTexture;
				break;
			case 3:
				thisPowerUp = "extraBall";
				this.renderer.material.mainTexture = extraBallTexture;
				break;		
			case 4:
				thisPowerUp = "speed";
				this.renderer.material.mainTexture = speedTexture;
				break;	
			default:
				thisPowerUp = "None";
				this.renderer.material.mainTexture = defaultTexture;
				break;
			}
			
			this.renderer.light.enabled = true;
		}
		else
		{
			this.renderer.material.mainTexture = defaultTexture;
			this.renderer.light.enabled = false;
		}
			
	}
	
	
	void OnTriggerEnter (Collider collidedObject) {
		
		if (Time.time > thisCooldownTime && collidedObject.gameObject.name == "Pinball(Clone)" && gameManagerComponent.getZoneIsAlive(this.transform.root.name)){
			
			if (thisPowerUp == "extraBall") {
				gameManagerComponent.AddPinball();
			}
			
			else if (thisPowerUp == "spawn") {
				Rigidbody newPinball = (Rigidbody) Instantiate(pinball, this.transform.position, this.transform.rotation);
				gameManagerComponent.spawnPinball();
			}
			
			else {			
				gameManagerComponent.setPowerUp(thisPowerUp);	
			}
			
			thisCooldownTime = Time.time + 30;
			
			powerUpAudio.Play();
		}
	}
	
}
