using UnityEngine;
using System.Collections;

public class DrainScript : MonoBehaviour {
	
	GameObject gameManager;
	GameManager gameManagerScript;
	
	// Use this for initialization
	void Start () {
		
		gameManager = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerScript = gameManager.GetComponent<GameManager>();
	
	}
	
	
	
	// Update is called once per frame
	void Update () {
	
	}
	
	
	
	void OnCollisionEnter(Collision collidedObject) {
		

		Destroy(collidedObject.gameObject);	
		if (collidedObject.transform.tag == "Pinball")
		{
			gameManagerScript.RemovePinballFromPlay();
		}
	}
	
}
