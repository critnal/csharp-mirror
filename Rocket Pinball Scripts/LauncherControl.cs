using UnityEngine;
using System.Collections;

public class LauncherControl : MonoBehaviour {
	
	public GameObject rocket;
	public GameObject rocketSpawnPoint;	
	
	GameObject gameManagerObject;
	GameManager gameManagerScript;
	
	public float rocketDelay;	
	
	private float lastFired;
	private bool canFire;
	
	AudioSource launcherAudio;
	
	// Use this for initialization
	void Awake () {
		gameManagerObject = GameObject.FindGameObjectWithTag("GameManager");
		gameManagerScript = gameManagerObject.GetComponent<GameManager>();
		
		lastFired = Time.time;
		canFire = true;
		
		launcherAudio = GetComponent(typeof(AudioSource)) as AudioSource;
	}
	
	// Update is called once per frame
	void Update () {		
				
		rocketSpawnPoint.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, 0);
		
		// If we want rockets to fire while the mouse is held down:
		if (!gameManagerScript.getUseTapFire() ) {
		
			if((Input.GetMouseButton(0)) && gameManagerScript.GetPlay ()){
				if (Time.time >= (lastFired + rocketDelay)){
					lastFired = Time.time;
					ShootRocket();
				}
			}
		}
		
		// Otherwise, if we want only one rocket to fire per LMB tap:
		else {
			
			if (Input.GetMouseButtonUp(0))
				canFire = true;
			
			if(Input.GetMouseButton(0) && (canFire) && gameManagerScript.GetPlay ()){
				if (Time.time >= (lastFired + rocketDelay)){
					lastFired = Time.time;
					ShootRocket();
					canFire = false;
				}
			}
			
		}
		
		
	}
	

	
	void ShootRocket() {
		Instantiate (rocket, rocketSpawnPoint.transform.position, this.transform.rotation);
		launcherAudio.Play ();
	}
	
}
