﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

namespace SharedGamesClasses {
    class Pack {
        public static int NUM_CARDS = 52;
        public static int NUM_SUITS = 4;
        private Card[] pack = new Card[NUM_CARDS];
        private int nextCard = 0;

        private static Random random = new Random();





        /// <summary>
        /// Constructor - initializes a pack of cards to 52 playing cards
        /// </summary>
        public Pack() {
            int index = 0;

            // Add 52 cards - One card of each FaceValue and Suit combination
            // to the pack array.
            for (FaceValue cardValue = FaceValue.Two; cardValue <= FaceValue.Ace; cardValue++) {
                for (Suit suit = Suit.Clubs; suit <= Suit.Spades; suit++) {
                    pack[index++] = new Card(suit, cardValue);
                }
            }
        }





        /// <summary>
        /// Shuffles a pack of cards
        /// Pre:   true
        /// Post:  cards in the pack list are rearranged into a random order
        /// </summary>
        public void Shuffle() {

            Card tempCard;
            int rNum;

            nextCard = 0;

            for (int i = pack.Length - 1; i > 0; i--) {

                rNum = random.Next(i + 1);

                tempCard = pack[i];  
                pack[i] = pack[rNum];
                pack[rNum] = tempCard;

            }

        } //end Shuffle





        /// <summary>
        /// Deals the next card in the pack
        /// Pre: true
        /// Post: returns the next card in the pack
        /// </summary>
        /// <returns></returns>
        public Card DealOneCard() {
           
            return pack[nextCard++];

        }//end DealOneCard





        /// <summary>
        /// Deals the next two cards in the pack
        /// Pre: true
        /// Post: Top two cards of the pack have been dealt
        /// </summary>
        /// <returns> The next two cards in the pack</returns>
        public ArrayList DealTwoCards() {
            ArrayList cards = new ArrayList();

            cards.Add(pack[nextCard++]);
            cards.Add(pack[nextCard++]);

            return cards;

        } //end DealTwoCards         
    
    }//end class Pack
}//end namespace
