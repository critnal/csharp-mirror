﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedGamesClasses {
    public enum Suit { Clubs, Diamonds, Hearts, Spades }

    public enum FaceValue {
        Two, Three, Four, Five, Six, Seven, Eight, Nine,
        Ten, Jack, Queen, King, Ace
    }

    /// <summary>
    /// Class to represent playing card with a Suit and FaceValue (pips).
    /// 
    /// Based on an earlier Java program written in 2002 for Software Development 1 (ITB410)
    /// 
    /// Default Constructor allows application to have "blank" cards 
    /// 
    /// Mike Roggenkamp
    /// </summary>
    public class Card {
        private Suit suit;
        private FaceValue faceValue;

       //Default constructor: Card is "blank" 
        public Card() {

        }

        // Constructor
        public Card(Suit suit, FaceValue faceValue) {
            this.suit = suit;
            this.faceValue = faceValue;
        }

        
        // ACCESSORS ONLY

        public Suit GetSuit() {
            return suit;
        }

        public FaceValue GetFaceValue() {
                return faceValue;
        }

    } //end class Card
}//end namespace
